package com.example.randomcocktails.usecases

import com.example.randomcocktails.usecases.drink_use_case.GetDrinksUseCase
import javax.inject.Inject

class UseCase @Inject constructor(
    val getDrinksUseCase: GetDrinksUseCase
){
}