package com.example.randomcocktails.usecases.drink_use_case

import com.example.randomcocktails.data.local.repo.Repository
import com.example.randomcocktails.model.apimodels.Drink

class GetDrinksUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(): List<Drink> {
        return repository.getDrinks()
    }
}