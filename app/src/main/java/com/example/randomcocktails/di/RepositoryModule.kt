package com.example.randomcocktails.di

import com.example.randomcocktails.data.local.repo.Repository
import com.example.randomcocktails.usecases.UseCase
import com.example.randomcocktails.usecases.drink_use_case.GetDrinksUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    @Singleton
    fun providesUseCase(repository: Repository): UseCase{
        return UseCase(
            getDrinksUseCase = GetDrinksUseCase(repository)
        )
    }
}