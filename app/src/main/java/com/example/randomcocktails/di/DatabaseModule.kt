package com.example.randomcocktails.di

import android.content.Context
import androidx.room.Room
import com.example.randomcocktails.data.local.db.RoomLocalDb
import com.example.randomcocktails.data.local.repo.LocalRepo
import com.example.randomcocktails.data.local.repo.LocalRepoImpl
import com.example.randomcocktails.model.CocktailRepo
import com.example.randomcocktails.model.CocktailRepoImpl
import com.example.randomcocktails.util.constants.Constants.DB_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun providesDatabase(
        @ApplicationContext context: Context
    ) : RoomLocalDb {
        return Room.databaseBuilder(
            context,
            RoomLocalDb::class.java,
            DB_NAME
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun providesLocalData(database: RoomLocalDb): LocalRepo {
        return LocalRepoImpl(database)
    }
}