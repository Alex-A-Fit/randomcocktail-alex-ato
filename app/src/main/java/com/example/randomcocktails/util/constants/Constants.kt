package com.example.randomcocktails.util.constants

object Constants {
    const val DB_NAME = "DRINK_DB"
    const val DRINK_TABLE = "DRINK_TABLE"
}