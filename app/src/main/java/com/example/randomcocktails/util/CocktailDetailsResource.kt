package com.example.randomcocktails.util

import com.example.randomcocktails.model.apimodels.Drink

sealed class CocktailDetailsResource(data: Drink?, errorMsg: String?){
    data class Success(val data: Drink): CocktailDetailsResource(data, null)
    object Loading: CocktailDetailsResource(null, null)
    data class Error(val errorMsg: String ): CocktailDetailsResource(null, errorMsg)
}
