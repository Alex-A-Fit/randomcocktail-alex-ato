package com.example.randomcocktails.util

import com.example.randomcocktails.model.apimodels.Drink

sealed class CocktailPreviewResource(data: List<Drink>?, errorMsg: String?){
    data class Success(val data: List<Drink>): CocktailPreviewResource(data, null)
    object Loading: CocktailPreviewResource(null, null)
    data class Error(val errorMsg: String ): CocktailPreviewResource(null, errorMsg)
}
