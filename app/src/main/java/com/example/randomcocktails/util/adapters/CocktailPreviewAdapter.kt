package com.example.randomcocktails.util.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.randomcocktails.databinding.CocktailItemBinding
import com.example.randomcocktails.model.apimodels.Drink

class CocktailPreviewAdapter(
        private val navigate: (id: String)
    -> Unit
) : RecyclerView
.Adapter<CocktailPreviewAdapter
.ViewHolder>() {

    private lateinit var cocktailList: List<Drink>

    class ViewHolder(private val binding: CocktailItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun applyCocktailData(drink: Drink) = with(binding) {
            cocktailImageIv.loadImage(drink.strDrinkThumb)
            cocktailNameTv.text = drink.strDrink
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            CocktailItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cocktail = cocktailList[position]
        holder.itemView.setOnClickListener {
            navigate(cocktail.idDrink)
        }
        holder.applyCocktailData(cocktail)
    }

    override fun getItemCount(): Int {
        return cocktailList.size
    }

    fun applyCocktailData(data: List<Drink>) {
        cocktailList = data
    }

}

private fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}