package com.example.randomcocktails.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.randomcocktails.R
import com.example.randomcocktails.databinding.FragmentCocktailDetailsBinding
import com.example.randomcocktails.databinding.FragmentRandomCocktailsBinding
import com.example.randomcocktails.model.apimodels.Drink
import com.example.randomcocktails.util.CocktailDetailsResource
import com.example.randomcocktails.viewmodel.CocktailDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CocktailDetailsFragment : Fragment() {
    private var _binding: FragmentCocktailDetailsBinding? = null
    private val binding: FragmentCocktailDetailsBinding get() = _binding!!

    val args by navArgs<CocktailDetailsFragmentArgs>()

    private val viewmodel by viewModels<CocktailDetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCocktailDetailsBinding.inflate(inflater, container, false).also {
        _binding = it
        viewmodel.getRandomCocktail(args.id)
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitViews()
    }

    private fun onInitViews() = with(binding){
        cocktailSV.isVisible = false
        viewmodel.viewState.observe(viewLifecycleOwner){ viewState ->
            when(viewState){
                is CocktailDetailsResource.Error -> {}
                CocktailDetailsResource.Loading -> {
                    binding.pbCircularLoading.visibility = View.VISIBLE
                    binding.tvLoadingText.isVisible = true
                }
                is CocktailDetailsResource.Success -> {
                    binding.pbCircularLoading.visibility = View.INVISIBLE
                    binding.tvLoadingText.isVisible = false
                    binding.cocktailSV.isVisible = true
                    val drink = viewState.data
                    cocktailImageIv.loadImage(drink.strDrinkThumb)
                    drinkTitle.text = drink.strDrink
                    if (drink.strAlcoholic == "Alcoholic"){
                        "Yes".also { alcoholLabelTextTv.text = it }
                    }else{
                        "No".also { alcoholLabelTextTv.text = it }
                    }
                    alcoholGlassTextTv.text = drink.strGlass
                    cocktailInstrucTv.text = drink.strInstructions
                    determineIngredients(drink)
                }
            }

        }
    }

    private fun determineIngredients(drink: Drink) = with(binding){
        when{
            drink.strIngredient2 == null || drink.strIngredient2.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                ingredient7.textView.visibility = View.GONE
                ingredient7.bullet.visibility = View.GONE
                ingredient6.textView.visibility = View.GONE
                ingredient6.bullet.visibility = View.GONE
                ingredient5.textView.visibility = View.GONE
                ingredient5.bullet.visibility = View.GONE
                ingredient4.textView.visibility = View.GONE
                ingredient4.bullet.visibility = View.GONE
                ingredient3.textView.visibility = View.GONE
                ingredient3.bullet.visibility = View.GONE
                ingredient2.textView.visibility = View.GONE
                ingredient2.bullet.visibility = View.GONE
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient3 == null || drink.strIngredient3.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                ingredient7.textView.visibility = View.GONE
                ingredient7.bullet.visibility = View.GONE
                ingredient6.textView.visibility = View.GONE
                ingredient6.bullet.visibility = View.GONE
                ingredient5.textView.visibility = View.GONE
                ingredient5.bullet.visibility = View.GONE
                ingredient4.textView.visibility = View.GONE
                ingredient4.bullet.visibility = View.GONE
                ingredient3.textView.visibility = View.GONE
                ingredient3.bullet.visibility = View.GONE
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient4 == null || drink.strIngredient4.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                ingredient7.textView.visibility = View.GONE
                ingredient7.bullet.visibility = View.GONE
                ingredient6.textView.visibility = View.GONE
                ingredient6.bullet.visibility = View.GONE
                ingredient5.textView.visibility = View.GONE
                ingredient5.bullet.visibility = View.GONE
                ingredient4.textView.visibility = View.GONE
                ingredient4.bullet.visibility = View.GONE
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient5 == null || drink.strIngredient5.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                ingredient7.textView.visibility = View.GONE
                ingredient7.bullet.visibility = View.GONE
                ingredient6.textView.visibility = View.GONE
                ingredient6.bullet.visibility = View.GONE
                ingredient5.textView.visibility = View.GONE
                ingredient5.bullet.visibility = View.GONE
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient6 == null || drink.strIngredient6.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                ingredient7.textView.visibility = View.GONE
                ingredient7.bullet.visibility = View.GONE
                ingredient6.textView.visibility = View.GONE
                ingredient6.bullet.visibility = View.GONE
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient7 == null || drink.strIngredient7.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                ingredient7.textView.visibility = View.GONE
                ingredient7.bullet.visibility = View.GONE
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient8 == null || drink.strIngredient8.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                ingredient8.textView.visibility = View.GONE
                ingredient8.bullet.visibility = View.GONE
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient9 == null || drink.strIngredient9.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                ingredient9.textView.visibility = View.GONE
                ingredient9.bullet.visibility = View.GONE
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient10 == null || drink.strIngredient10.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                ingredient10.textView.visibility = View.GONE
                ingredient10.bullet.visibility = View.GONE
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient11 == null || drink.strIngredient11.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                ingredient11.textView.visibility = View.GONE
                ingredient11.bullet.visibility = View.GONE
                (drink.strMeasure11 + " " + drink.strIngredient11).also { ingredient11.textView.text = it }
                (drink.strMeasure10 + " " + drink.strIngredient10).also { ingredient10.textView.text = it }
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient12 == null || drink.strIngredient12.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                ingredient12.textView.visibility = View.GONE
                ingredient12.bullet.visibility = View.GONE
                (drink.strMeasure11 + " " + drink.strIngredient11).also { ingredient11.textView.text = it }
                (drink.strMeasure10 + " " + drink.strIngredient10).also { ingredient10.textView.text = it }
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient13 == null || drink.strIngredient13.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                ingredient13.textView.visibility = View.GONE
                ingredient13.bullet.visibility = View.GONE
                (drink.strMeasure12 + " " + drink.strIngredient12).also { ingredient12.textView.text = it }
                (drink.strMeasure11 + " " + drink.strIngredient11).also { ingredient11.textView.text = it }
                (drink.strMeasure10 + " " + drink.strIngredient10).also { ingredient10.textView.text = it }
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient14 == null || drink.strIngredient14.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                ingredient14.textView.visibility = View.GONE
                ingredient14.bullet.visibility = View.GONE
                (drink.strMeasure13 + " " + drink.strIngredient13).also { ingredient13.textView.text = it }
                (drink.strMeasure12 + " " + drink.strIngredient12).also { ingredient12.textView.text = it }
                (drink.strMeasure11 + " " + drink.strIngredient11).also { ingredient11.textView.text = it }
                (drink.strMeasure10 + " " + drink.strIngredient10).also { ingredient10.textView.text = it }
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }
            }
            drink.strIngredient15 == null || drink.strIngredient15.isEmpty() -> {
                ingredient15.textView.visibility = View.GONE
                ingredient15.bullet.visibility = View.GONE
                (drink.strMeasure14 + " " + drink.strIngredient14).also { ingredient14.textView.text = it }
                (drink.strMeasure13 + " " + drink.strIngredient13).also { ingredient13.textView.text = it }
                (drink.strMeasure12 + " " + drink.strIngredient12).also { ingredient12.textView.text = it }
                (drink.strMeasure11 + " " + drink.strIngredient11).also { ingredient11.textView.text = it }
                (drink.strMeasure10 + " " + drink.strIngredient10).also { ingredient10.textView.text = it }
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }

            }
            else -> {
                (drink.strMeasure15 + " " + drink.strIngredient15).also { ingredient15.textView.text = it }
                (drink.strMeasure14 + " " + drink.strIngredient14).also { ingredient14.textView.text = it }
                (drink.strMeasure13 + " " + drink.strIngredient13).also { ingredient13.textView.text = it }
                (drink.strMeasure12 + " " + drink.strIngredient12).also { ingredient12.textView.text = it }
                (drink.strMeasure11 + " " + drink.strIngredient11).also { ingredient11.textView.text = it }
                (drink.strMeasure10 + " " + drink.strIngredient10).also { ingredient10.textView.text = it }
                (drink.strMeasure9 + " " + drink.strIngredient9).also { ingredient9.textView.text = it }
                (drink.strMeasure8 + " " + drink.strIngredient8).also { ingredient8.textView.text = it }
                (drink.strMeasure7 + " " + drink.strIngredient7).also { ingredient7.textView.text = it }
                (drink.strMeasure6 + " " + drink.strIngredient6).also { ingredient6.textView.text = it }
                (drink.strMeasure5 + " " + drink.strIngredient5).also { ingredient5.textView.text = it }
                (drink.strMeasure4 + " " + drink.strIngredient4).also { ingredient4.textView.text = it }
                (drink.strMeasure3 + " " + drink.strIngredient3).also { ingredient3.textView.text = it }
                (drink.strMeasure2 + " " + drink.strIngredient2).also { ingredient2.textView.text = it }
                (drink.strMeasure1 + " " + drink.strIngredient1).also { ingredient1.textView.text = it }

            }
        }
    }

}



private fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}