package com.example.randomcocktails.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.randomcocktails.databinding.FragmentRandomCocktailsBinding
import com.example.randomcocktails.model.apimodels.Drink
import com.example.randomcocktails.util.CocktailPreviewResource
import com.example.randomcocktails.util.adapters.CocktailPreviewAdapter
import com.example.randomcocktails.viewmodel.RandomCocktailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract
import kotlin.math.log

@AndroidEntryPoint
class RandomCocktailsFragment : Fragment() {
    private var _binding: FragmentRandomCocktailsBinding? = null
    private val binding: FragmentRandomCocktailsBinding get() = _binding!!

    private val viewmodel by viewModels<RandomCocktailsViewModel>()

    private lateinit var drink: Drink


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentRandomCocktailsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.pbCircularLoading.visibility = View.INVISIBLE
        binding.tvLoadingText.isVisible = false
        onInitListener()
    }

    private fun initObserver() = with(binding){
        viewmodel.getDrinkFromDb()
        recyclerView.layoutManager = LinearLayoutManager(context)

        viewmodel.drinks.observe(viewLifecycleOwner){ drinks ->
            recyclerView.adapter = CocktailPreviewAdapter(
                ::navigate
            ).apply {
                applyCocktailData(drinks)
            }

        }
    }

    private fun onInitListener() = with(binding) {
        button.setOnClickListener {
            initObserver()
        }
    }
    private fun navigate(id: String) {
        val action = RandomCocktailsFragmentDirections.actionRandomCocktailsFragmentToCocktailDetailsFragment(id)
        findNavController().navigate(action)
    }

}

