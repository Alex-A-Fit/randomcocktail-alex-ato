package com.example.randomcocktails.data.local.repo

import com.example.randomcocktails.data.local.db.RoomLocalDb
import com.example.randomcocktails.model.apimodels.Drink

class LocalRepoImpl(database: RoomLocalDb): LocalRepo {
    private val localCocktailDao = database.localCocktailDao()

    override suspend fun getCocktailsFromDB(): List<Drink>{
        return localCocktailDao.getDrink()
    }

    override suspend fun addDrinkToDb(drink: Drink) {
        return localCocktailDao.addDrinkToDb(drink)
    }

}