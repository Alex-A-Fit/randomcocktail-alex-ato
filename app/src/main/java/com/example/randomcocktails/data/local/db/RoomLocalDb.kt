package com.example.randomcocktails.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.randomcocktails.model.apimodels.Drink

@Database(entities = [Drink::class], exportSchema = false, version = 2)
abstract class RoomLocalDb: RoomDatabase() {
//    companion object {
//        const val DB_NAME = "DRINK_DB"
//        fun create(context:Context): RoomLocalDb {
//            return Room.databaseBuilder(context, RoomLocalDb::class.java, DB_NAME).fallbackToDestructiveMigration().build()
//        }
//    }
    abstract fun localCocktailDao(): LocalCocktailDao
}