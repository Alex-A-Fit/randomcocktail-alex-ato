package com.example.randomcocktails.data.local.repo

import com.example.randomcocktails.model.apimodels.Drink

interface LocalRepo {
    suspend fun getCocktailsFromDB(): List<Drink>

    suspend fun addDrinkToDb(drink: Drink)
}