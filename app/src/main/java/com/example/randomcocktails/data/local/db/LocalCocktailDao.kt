package com.example.randomcocktails.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.randomcocktails.model.apimodels.Drink

@Dao
interface LocalCocktailDao {
    @Query("SELECT * FROM drink_table")
    fun getDrink(): List<Drink>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addDrinkToDb(drink:Drink)
}