package com.example.randomcocktails.data.local.repo

import com.example.randomcocktails.model.apimodels.Drink
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class Repository @Inject constructor(
    private val local: LocalRepo
) {
    suspend fun getDrinks(): List<Drink> = withContext(Dispatchers.IO){
        return@withContext local.getCocktailsFromDB()
    }

    suspend fun addDrink(drink: Drink) = withContext(Dispatchers.IO){
        return@withContext local.addDrinkToDb(drink)
    }
}