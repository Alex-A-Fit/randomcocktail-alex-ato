package com.example.randomcocktails.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.randomcocktails.data.local.repo.Repository
import com.example.randomcocktails.model.CocktailRepoImpl
import com.example.randomcocktails.model.apimodels.Drink
import com.example.randomcocktails.usecases.UseCase
import com.example.randomcocktails.util.CocktailPreviewResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RandomCocktailsViewModel @Inject constructor(
    private val useCase: UseCase
): ViewModel() {
    private val repo by lazy { CocktailRepoImpl() }

    private val _drinks: MutableLiveData<List<Drink>> = MutableLiveData(mutableListOf())
    val drinks: LiveData<List<Drink>> get() = _drinks

    fun getDrinkFromDb() {
        viewModelScope.launch(Dispatchers.Main) {
            _drinks.value = useCase.getDrinksUseCase()
        }
    }
}