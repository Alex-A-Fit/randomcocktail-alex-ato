package com.example.randomcocktails.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.randomcocktails.model.CocktailRepoImpl
import com.example.randomcocktails.util.CocktailDetailsResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CocktailDetailsViewModel: ViewModel() {

    private val repo by lazy { CocktailRepoImpl() }

    private val _viewState: MutableLiveData<CocktailDetailsResource> = MutableLiveData(CocktailDetailsResource.Loading)
    val viewState: LiveData<CocktailDetailsResource> get() = _viewState

    fun getRandomCocktail(id: String) {
        viewModelScope.launch(Dispatchers.Main) {
            _viewState.value = repo.getCocktailDetailsById(id)
        }
    }
}