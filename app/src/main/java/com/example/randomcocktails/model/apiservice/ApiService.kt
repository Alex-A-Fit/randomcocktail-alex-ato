package com.example.randomcocktails.model.apiservice

import com.example.randomcocktails.model.apimodels.Cocktail
import com.example.randomcocktails.model.apimodels.Drink
import com.google.gson.annotations.SerializedName
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("random.php")
    suspend fun getRandomCocktail(): Response<Cocktail>

    @GET("lookup.php")
    suspend fun getCocktailDetailsById(@Query("i") i: String): Response<Cocktail>


    companion object{
        private val okhttp by lazy {
            OkHttpClient.Builder().addNetworkInterceptor(FlipperProvider.getFlipperOkhttpInterceptor()).build()
        }

        val retrofit by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://www.thecocktaildb.com/api/json/v1/1/")
                .client(okhttp)
                .build()
                .create(ApiService::class.java)
        }
    }
}