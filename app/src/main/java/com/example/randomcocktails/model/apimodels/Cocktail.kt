package com.example.randomcocktails.model.apimodels

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cocktail(
    val drinks: List<Drink>
): Parcelable