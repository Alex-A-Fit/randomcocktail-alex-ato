package com.example.randomcocktails.model

import com.example.randomcocktails.data.local.db.RoomLocalDb
import com.example.randomcocktails.model.apimodels.Drink
import com.example.randomcocktails.model.apiservice.ApiService
import com.example.randomcocktails.util.CocktailDetailsResource
import com.example.randomcocktails.util.CocktailPreviewResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class CocktailRepoImpl(): CocktailRepo {
    private val apiService by lazy { ApiService.retrofit }

    override suspend fun getRandomCocktail(): CocktailPreviewResource? = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = apiService.getRandomCocktail()
            val drink = response.body()?.drinks
            if (drink?.isNotEmpty()!!) {

                CocktailPreviewResource.Success(drink)
            } else {
                CocktailPreviewResource.Error("Response error on check")
            }
        } catch (e: Exception) {
            e.localizedMessage?.let { CocktailPreviewResource.Error(it) }
        }
    }

    override suspend fun getCocktailDetailsById(id: String): CocktailDetailsResource? = withContext(Dispatchers.IO){
        return@withContext try{
            val response = apiService.getCocktailDetailsById(id)
            if(response.isSuccessful){
                CocktailDetailsResource.Success(response.body()?.drinks?.get(0)!!)
            }else{
                CocktailDetailsResource.Error("Response error on check")
            }
        } catch (e: Exception) {
            e.localizedMessage?.let { CocktailDetailsResource.Error(it) }
        }
    }
}