package com.example.randomcocktails.model

import com.example.randomcocktails.util.CocktailDetailsResource
import com.example.randomcocktails.util.CocktailPreviewResource

interface CocktailRepo {
    suspend fun getRandomCocktail(): CocktailPreviewResource?

    suspend fun getCocktailDetailsById(id: String): CocktailDetailsResource?

}